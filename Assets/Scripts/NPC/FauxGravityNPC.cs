﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FauxGravityNPC : MonoBehaviour
{
    RaycastHit hit;
    public Vector3 gravityDir = -Vector3.up;

    //float minGravityAmount = 20f;
    //float maxGravityAmount = 30f;
    //float extremeGravityAmount = 80f;
    public float gravityAmount;
    public LayerMask mask;

    SoftBodyNPC softBody;
    Rigidbody rb;

    float velocityDecay = 0.1f;

    public Vector3 velocityTest;
    public Vector3 localVelocity;

    Vector3 lastGravityDir;

    float miniJumpCounter = 20f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        softBody = GetComponent<SoftBodyNPC>();
    }

    void Update()
    {
        velocityTest = rb.velocity;

        //Don't use this with an NPC.
        //SetGravitationalForce();  
        //if (Physics.Raycast(transform.position, -transform.up, out hit, 10f, mask, QueryTriggerInteraction.Ignore))
        //{
        //    gravityDir = -hit.normal;
        //    if(Vector3.Dot(gravityDir, lastGravityDir) < 0.99f)
        //    {
        //        AvoidMiniJump();
        //    }
        //    lastGravityDir = gravityDir;
        //}

        Rotate();

        //if (!softBody.grounded)
        //{
        rb.AddForce(gravityDir * gravityAmount);
        //}

    }

    private void AvoidMiniJump()
    {
        rb.AddForce(gravityDir * miniJumpCounter);

        print("no miniJumps!");
    }

    private void SetGravitationalForce()
    {
        //localVelocity = transform.InverseTransformDirection(rb.velocity);
        //float verticalVel = localVelocity.y;

        //if (verticalVel >= 0f)
        //{
        //    if (Input.GetButton("Jump"))
        //    {
        //        gravityAmount = minGravityAmount;
        //    }
        //    else
        //    {
        //        gravityAmount = extremeGravityAmount * (verticalVel) / 5f;
        //        if (gravityAmount < minGravityAmount) gravityAmount = minGravityAmount;
        //    }
        //}
        //else
        //{
        //    gravityAmount = maxGravityAmount;

        //}
    }

    private void Rotate()
    {
        Quaternion targetRotation = Quaternion.FromToRotation(transform.up, -gravityDir) * transform.rotation;
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 2f);
    }
}



//I could create a very big trigger. This could get the closest Gravitational-Object and it's closest point and use it's normal as the gravity dir.
//I could also add the gravitational pulls together based on distance.

    //IMPORTANT: Sphere collider doesn't actually give the normal of it's real collider! It gives an amount which changes even when on a different place on the same polygon!!
