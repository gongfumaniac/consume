﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class EyeMovementNPC : MonoBehaviour
{
    Transform[] eyes = new Transform[2];
    public Vector3[] eyeTargetOffsets = new Vector3[2];

    public AnimationCurve eyeTurnPercentage;


    float timer;

    float meshRadius;

    //MoveSoftBody moveScript;
    MoveSoftNPC moveScript;

    public Vector3 rollerRotation;

    Vector3 lastVelocityDirection;

    Vector3[] eyePositions = new Vector3[2];

    bool rotationReset;

    float rollResetTimer;

    public Vector3 rotationAxis; //Axis which is perpendicular to transform.up and globalPlanarVelocity.

    SoftBodyNPC softbody;

    public Vector3 test;
    Vector3 test2;

    public float dot;

    public float lerpSpeed;
    float maxLerpSpeed = 18f;
    float minLerpSpeed = 0.5f;

    public Vector3 localPlanarVelocity;

    Vector3 targetMiddleEyePosition;
    Vector3 velocityPosition;

    public float rollAmount;
    public float rollSpeed;
    float maxRollSpeed = 17f;

    WaitForEndOfFrame waitForFrame = new WaitForEndOfFrame();

    public Vector3 crossEyeOffset;
    float crossEyeAmount = 0.5f;

    public Vector3 movementVector;
    Vector3 lastMovementVector;

    public Vector3 rolledOffset;

    public Vector3 offsetDirection;
    int offsetInversion = 1;

    Vector3 velocity;

    public bool turning;
    public bool eyesAdjusting;

    public bool bufferingTurnInput;
    Vector3 bufferedInput;


    float standardEyeHeight = 25f; //in degrees

    Vector3 turningTargetPosition;

    float adjustmentBonus;

    Vector3 targetPos;

    bool startingSneak;
    bool sneaked;
    Vector3 entrancePos;
    Vector3 exitPos;




    private void Awake()
    {
        softbody = GetComponent<SoftBodyNPC>();

        moveScript = GetComponent<MoveSoftNPC>();

        meshRadius = GetComponent<SphereCollider>().radius;

        Transform eyeEmpty = transform.GetChild(0);
        for (int i = 0; i < 2; i++)
        {
            eyes[i] = eyeEmpty.GetChild(i).transform;
        }

        rollSpeed = maxRollSpeed;
        lerpSpeed = maxLerpSpeed;
    }

    public void SetEyePosition(Vector3 velocity, Vector3 localVelocity) //DON'T use magnitude! Use sqrMagnitude instead.
    {
        //Might look better if eye rotates quicker if near the ground.
        //the rolled offset can be converted into local space to finde the closest startVertices[v]. The eye should then move to the corresponding vertex[v]

        //Vector3 eyesHeigthOffset = transform.up * 0;

        Vector3 eyePosDifference = eyes[0].localPosition - eyes[1].localPosition;
        Vector3 middleEyePosition = eyes[1].localPosition + (eyePosDifference * 0.5f);


        localPlanarVelocity = localVelocity;
        localPlanarVelocity.y = 0f;


        float sqrPlanarMagnitude = localPlanarVelocity.sqrMagnitude;

        if (moveScript.movementVector.sqrMagnitude > 0.001f)
        {
            if (!turning)
            {
                movementVector = moveScript.movementVector.normalized;
            }
            else
            {
                bufferedInput = moveScript.movementVector.normalized;
                bufferingTurnInput = true;
            }

            dot = Vector3.Dot(lastMovementVector, movementVector);
            if (dot < 0.8f && !turning)
            {
                offsetInversion = -offsetInversion;
                TriggerEyesAdjusting();
            }

        }

        lastMovementVector = movementVector;
        if (bufferingTurnInput && !turning)
        {
            if (moveScript.movementVector.sqrMagnitude < 0.01f)
            {
                float bufferedDot = Vector3.Dot(middleEyePosition.normalized, bufferedInput.normalized);
                if (bufferedDot < 0.8f && !turning)
                {
                    offsetInversion = -offsetInversion;
                    TriggerEyesAdjusting();
                    movementVector = bufferedInput;
                    lastMovementVector = bufferedInput;//
                }
            }
            bufferingTurnInput = false;
        }

        if (sqrPlanarMagnitude > 0.05f)
        {
            lastVelocityDirection = localPlanarVelocity.normalized;


            if (!eyesAdjusting && !turning && lerpSpeed < maxLerpSpeed)
            {
                lerpSpeed += Time.deltaTime * 30f;
            }

            rollAmount += Time.deltaTime * rollSpeed * sqrPlanarMagnitude;
            if (rollAmount >= 360f)
            {
                rollAmount = 0f;
            }

            Vector3 globalPlanarVelocity = transform.TransformDirection(localPlanarVelocity);

            rotationAxis = Vector3.Cross(transform.up, globalPlanarVelocity.normalized);
        }
        else if (!eyesAdjusting && !turning)
        {

            float resetSpeed = 5f;

            if (rollAmount <= 90f)
            {
                lerpSpeed = maxLerpSpeed * 0.4f;
                rollAmount = Mathf.Lerp(rollAmount, 0f, Time.deltaTime * resetSpeed);
            }
            else if (rollAmount >= 270f)
            {
                lerpSpeed = maxLerpSpeed * 0.4f;

                rollAmount = Mathf.Lerp(rollAmount, 359f, Time.deltaTime * resetSpeed);
            }
            else
            {
                TriggerEyesAdjusting();
            }
        }


        if (lerpSpeed < maxLerpSpeed * 1f) // I may need to restrict this.
        {
            lerpSpeed += Time.deltaTime * 30f;
        }
        else if (lerpSpeed > maxLerpSpeed)
        {
            lerpSpeed = maxLerpSpeed;
        }




        rolledOffset = Quaternion.AngleAxis(rollAmount - standardEyeHeight, -offsetDirection) * lastMovementVector;

        if (turning)
        {
            targetMiddleEyePosition = turningTargetPosition;
        }
        else
        {
            targetMiddleEyePosition = rolledOffset;
            offsetDirection = Vector3.Cross(movementVector, Vector3.up);


            crossEyeOffset = offsetDirection.normalized * crossEyeAmount;
        }


        adjustmentBonus = 1f;
        if (eyesAdjusting)
        {
            adjustmentBonus = 1.08f;
            Vector3 actualTargetPos = GetNearestVertex(targetMiddleEyePosition);

            float arrivalDistance = 0.15f; //this is very unclean because actual target Pos is multiplied by 1.2
            if ((actualTargetPos - middleEyePosition).magnitude < arrivalDistance) //this seems to be true all the time!
            {
                eyesAdjusting = false;
                StartCoroutine(ChangeValue((result) => { rollSpeed = result; }, 0f, maxRollSpeed, 60f));  //The sudden change to max rollSpeed is problematic. If it increased more slowly it would look awkward as well.
                StartCoroutine(ChangeValue((result) => { lerpSpeed = result; }, lerpSpeed, maxLerpSpeed, 15f));
            }
        }

        crossEyeOffset *= offsetInversion;

        StartApplyingEyePositions();


        if (crossEyeOffset.magnitude < 0.1f)
        {
            offsetDirection = Vector3.Cross(movementVector, Vector3.up);
            //print(crossEyeOffset + " Magnitude: " + crossEyeOffset.magnitude);
        }

        test = crossEyeOffset + transform.position;
    }

    IEnumerator Turning(Vector3 startPos, Vector3 targetPos)
    {

        //turning should only be triggered if turning on the y axis. at the start of the game it may just happen because my input vectors are (0,0,0) when the game starts.

        //If rotationAmount is high so should speed.


        if (turning == true)
        {
            Debug.LogWarning("This Coroutine has been called even though the bool turning was already true. That should never happen.");
            yield break;
        }

        turning = true;


        float speed = 250f;

        float actualPercentage = 0f;
        Quaternion rotation = Quaternion.FromToRotation(startPos, targetPos);

        float rotationAmount = rotation.eulerAngles.y;

        if (rotationAmount > 180f)
        {
            rotationAmount -= 360f;
        }

        //Debug.Log("Turning() started with these values: StartPos " + startPos + " ; TargetPos " + targetPos + " , Rotation " + rotation.eulerAngles + " , rotationAmount " + rotationAmount);

        while (actualPercentage < 1f)
        {
            actualPercentage += Time.deltaTime * speed / 100;

            float animatedPercentage = eyeTurnPercentage.Evaluate(actualPercentage);

            offsetDirection = Vector3.Cross(turningTargetPosition, Vector3.up).normalized;
            crossEyeOffset = offsetDirection * crossEyeAmount;

            turningTargetPosition = Quaternion.AngleAxis(rotationAmount * animatedPercentage, Vector3.up) * startPos;
            turningTargetPosition.y = 0f;
            turningTargetPosition = Quaternion.AngleAxis(0 - standardEyeHeight, -offsetDirection.normalized) * turningTargetPosition;
            turningTargetPosition = turningTargetPosition.normalized * 1f;

            offsetDirection = Vector3.Cross(turningTargetPosition, transform.up);

            yield return waitForFrame;
        }

        turningTargetPosition = Quaternion.AngleAxis(rotationAmount * actualPercentage, transform.up) * startPos;
        turningTargetPosition.y = 0f;
        turningTargetPosition = Quaternion.AngleAxis(0 - standardEyeHeight, -offsetDirection.normalized) * turningTargetPosition;
        turningTargetPosition = turningTargetPosition.normalized * 1f;
        turning = false;

        rollAmount = 0f;
    }

    void TriggerEyesAdjusting()
    {
        eyesAdjusting = true;
        rollAmount = 0f;
        rollSpeed = 0f;
        lerpSpeed = minLerpSpeed;
    }

    void StartApplyingEyePositions()
    {
        Vector3 targetPosition = GetNearestVertex(targetMiddleEyePosition + crossEyeOffset) * adjustmentBonus;
        ApplyEyePosition(0, targetPosition, velocity);
        targetPosition = GetNearestVertex(targetMiddleEyePosition - crossEyeOffset) * adjustmentBonus;
        ApplyEyePosition(1, targetPosition, velocity);
    }

    void ApplyEyePosition(int index, Vector3 position, Vector3 velocity)
    {
        float turnSpeed = 50f;

        eyes[index].localPosition = Vector3.Lerp(eyes[index].localPosition, position, Time.deltaTime * lerpSpeed);

        //Quaternion lookDirection = Quaternion.LookRotation(eyes[index].localPosition, Vector3.up);
        Quaternion lookDirection = Quaternion.LookRotation(eyes[index].localPosition, transform.up); //I may have to completely rewrite this.

        eyes[index].localRotation = Quaternion.Lerp(eyes[index].rotation, lookDirection, Time.deltaTime * turnSpeed);
    }

    IEnumerator ChangeValue(Action<float> callback, float val, float targetVal, float increment)
    {

        while ((targetVal - val) > increment)
        {
            val += increment * Time.deltaTime;
            callback(val);
            yield return waitForFrame;
        }



        while ((targetVal - val) < -increment)
        {
            val -= increment * Time.deltaTime;
            callback(val);

            yield return waitForFrame;
        }

        val = targetVal;
        callback(val);
    }



    Vector3 GetNearestVertex(Vector3 position)
    {
        float leastDistance = 100000f;
        int closestVertex = 0;

        for (int i = 0; i < softbody.vertices.Length; i++)
        {
            float sqrDistance = (position - softbody.startVertices[i]).sqrMagnitude;

            if (sqrDistance < leastDistance)
            {
                leastDistance = sqrDistance;
                closestVertex = i;
            }
        }

        return softbody.vertices[closestVertex];
    }


    public void AirborneEyePosition(Vector3 localVelocity)
    {
        rollAmount = 0f;

        Vector3 eyePosDifference = eyes[0].localPosition - eyes[1].localPosition;
        Vector3 middleEyePosition = eyes[1].localPosition + (eyePosDifference * 0.5f);


        if (localVelocity.sqrMagnitude < 0.05f)
        {
            return;
        }

        targetMiddleEyePosition = localVelocity.normalized;

        float dot = Vector3.Dot(middleEyePosition.normalized, targetMiddleEyePosition.normalized);

        if(dot < 0.5f)
        {
            lerpSpeed = maxLerpSpeed * 0.25f;
        }
        else if(lerpSpeed < maxLerpSpeed)
        {
            lerpSpeed += Time.deltaTime * 10f;
        }

        Vector3 newOffset = Vector3.Cross(localVelocity, Vector3.up);
        if (newOffset.sqrMagnitude > 0.1f)
        {
            offsetDirection = newOffset;

        }


        crossEyeOffset = offsetDirection.normalized * crossEyeAmount * offsetInversion;

        adjustmentBonus = 1f;
        StartApplyingEyePositions();
    }



    //public IEnumerator StartSneaking()
    //{
    //    Vector3 eyePosDifference = eyes[0].localPosition - eyes[1].localPosition;
    //    Vector3 middleEyePosition = eyes[1].localPosition + (eyePosDifference * 0.5f);

    //    sneaked = true;
    //    entrancePos = middleEyePosition;

    //    rollAmount = 0f;

    //    lerpSpeed = maxLerpSpeed * 0.05f;
    //    startingSneak = true;

    //    Vector3 localMovementvector = moveScript.movementVector.normalized;

    //    if (localMovementvector.sqrMagnitude > 0.2f)
    //    {
    //        movementVector = localMovementvector;
    //    }

    //    targetMiddleEyePosition = localMovementvector;


       

    //    while ((targetMiddleEyePosition - middleEyePosition).sqrMagnitude > 0.3f)
    //    {
    //        eyePosDifference = eyes[0].localPosition - eyes[1].localPosition;
    //        middleEyePosition = eyes[1].localPosition + (eyePosDifference * 0.5f);

    //        StartApplyingEyePositions();

    //        yield return waitForFrame;
    //    }

    //    startingSneak = false;
    //    yield return null;
    //}

    public void SneakingEyePosition()
    {
        targetPos = GetComponent<Rigidbody>().velocity;
        lerpSpeed = maxLerpSpeed;


        //if (startingSneak) return;

        //lerpSpeed = maxLerpSpeed / 2f;
        //float speed = 150f;

        //Vector3 eyePosDifference = eyes[0].localPosition - eyes[1].localPosition;
        //Vector3 middleEyePosition = eyes[1].localPosition + (eyePosDifference * 0.5f);

        //Vector3 localMovementvector = moveScript.movementVector.normalized;
        //if(localMovementvector.sqrMagnitude > 0.1f)
        //{
        //    movementVector = localMovementvector;
        //}
        //lastMovementVector = movementVector;

        //Quaternion rotation = Quaternion.FromToRotation(middleEyePosition.normalized, movementVector.normalized);
        //float rotationAmount = rotation.eulerAngles.y;
        //if (rotationAmount < 3f) return;

        //if (rotationAmount > 180f)
        //{
        //    rotationAmount -= 360f;
        //}

        //bool clockwise = true;
        //int rotationDir = 1;
        //if(rotationAmount < 0f)
        //{
        //    clockwise = false;
        //    rotationDir = -1;
        //}
        //float maxRotationAmount = 45f;
        //rotationAmount = Mathf.Clamp(rotationAmount, -maxRotationAmount, maxRotationAmount);
        //rotationAmount = Mathf.Abs(rotationAmount / maxRotationAmount) + 1;

        //targetPos = Quaternion.AngleAxis(speed * Time.deltaTime * rotationAmount * rotationDir, Vector3.up) * targetMiddleEyePosition;

        targetPos.y = 0f;
        //targetPosition = Quaternion.AngleAxis(0 - standardEyeHeight, -offsetDirection.normalized) * turningTargetPosition;
        targetPos = targetPos.normalized * 1f;

        offsetDirection = Vector3.Cross(targetPos, Vector3.up).normalized;
        crossEyeOffset = offsetDirection * crossEyeAmount * offsetInversion;

        targetMiddleEyePosition = targetPos;
        StartApplyingEyePositions();

        //Debug.DrawRay(transform.position, offsetDirection, Color.black);

        //test = crossEyeOffset + transform.position;
        ////Debug.Log("rotationAmount " + rotationAmount + " middleEyePosition " + middleEyePosition + " targetMiddleEyePos " + targetMiddleEyePosition + " targetPos " + targetPos);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        //Gizmos.DrawSphere(eyes[0].position, 0.2f);
        Gizmos.DrawSphere(test, 0.1f);
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(test2, 0.1f);
    }
}

//If the eye is below the hit point of the ground it should move to the hit point + a little offset in transform.up direction.
//If Softbody isn't grounded it's eyes should just move to the velocity position. If it is grounded and not moving it should move to the last planar velocity position AFTER a delay.

//I could make the eyes look in the direction of their respective normals.

//offsetDirection should be Vector3.Cross(middleEyePosition, Vector3.up), I think. May not be a noticeable difference though.
