﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    Vector3 moveDir;

    float h;
    float v;

    Transform lastTouched;
    Transform meshes;

    Vector3 testNormal;

    private void Awake()
    {
        meshes = transform.GetChild(0);
    }


    void FixedUpdate()
    {
        PlayerMovement();


        if (Physics.Raycast(transform.position, -transform.up, out hit, 1.5f))
        {
            //transform.rotation = Quaternion.LookRotation(transform.forward, hit.normal);

            Vector3 gravityDir = hit.normal;
            Quaternion targetRotation = Quaternion.FromToRotation(transform.up, gravityDir) * transform.rotation;
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 8f);

            transform.position -= (transform.position - hit.point) * 0.05f;

            if (moveDir.normalized.magnitude > 0.9)
            meshes.rotation = Quaternion.LookRotation(transform.TransformDirection(moveDir), gravityDir);

            if (lastTouched != hit.transform)
            {
                lastTouched = hit.transform;
            }
        }
        else
        {
            if(lastTouched != null)
            {
                //Vector3 gravityDir = transform.position - lastTouched.position;
                Vector3 gravityDir = hit.normal;

                transform.position -= gravityDir * 0.1f;

                Quaternion targetRotation = Quaternion.FromToRotation(transform.up, gravityDir) * transform.rotation;
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 2f);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(Jump());
        } 
        //Round planet gravity
        //transform.position -= (transform.position - planet.transform.position) * 0.01f;
    }

    IEnumerator Jump()
    {
        float velocity = 0.2f;

        while(velocity >= 0f)
        {
            velocity -= 0.01f;
            transform.position += transform.up;
        yield return new WaitForEndOfFrame();
        }
    }

    public void PlayerMovement()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        moveDir = new Vector3(h, 0f, v);

        transform.position += transform.TransformDirection(moveDir.normalized * 0.1f);

        if (Input.GetKeyDown(KeyCode.Space)) print(testNormal);
    }

    private void OnCollisionEnter(Collision other)
    {
        testNormal = other.contacts[other.contacts.Length - 1].normal;
    }
}
