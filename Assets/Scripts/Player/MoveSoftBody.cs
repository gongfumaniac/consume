﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSoftBody : MonoBehaviour
{
    Rigidbody rb;

    [HideInInspector]
    public Vector3 movementVector;
    [HideInInspector]
    public Vector3 globalMovementVector;


    //public Vector3 newVelocity;
    //public Vector3 testGlobal;
    //public Vector3 testLocal;


    Vector3 localVelocity;
    Vector3 globalVelocity;

    SoftBodyTest6 softBody;
    float maxSneakingSlowDebuff = 0.25f;
    public float movementClamp = 1f;

    float maxSpeed = 5f;

    float movementSpeed = 11f;

    float jumpForce = 800f;

    float planarDrag = 0.04f;
    float verticalDrag = 0.035f;

    void Start()
    {
        softBody = GetComponent<SoftBodyTest6>();
        rb = GetComponent<Rigidbody>();
    }


    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        movementVector = new Vector3(h, 0f, v);


        if (softBody.sneaking)
        {
            float sneakingSlowDebuff = maxSneakingSlowDebuff / (softBody.sneakAmount);
            movementClamp = Mathf.Lerp(movementClamp, sneakingSlowDebuff, Time.deltaTime * 5f);
        }
        else
        {
            movementClamp = 1f;
        }

        globalMovementVector = transform.TransformDirection(movementVector);
        globalMovementVector = Vector3.ClampMagnitude(globalMovementVector, movementClamp);



        if (Input.GetButtonDown("Jump") && (softBody.grounded || softBody.onLedge))
        {
            rb.AddForce(transform.up * jumpForce);
        }

        globalVelocity = rb.velocity;
        localVelocity = transform.InverseTransformDirection(globalVelocity);
        Vector3 planarVelocity = localVelocity;
        planarVelocity.y = 0f;

        if (planarVelocity.sqrMagnitude < maxSpeed * maxSpeed)
        {
            rb.AddForce(globalMovementVector * movementSpeed);
        }

        //this creates very weird bugs when gravityDir is changed. I have to fix it because it is essential for movement (it replaces Unity's rigidbody.drag.)

        Vector3 newVelocity = new Vector3(localVelocity.x * (1f - planarDrag), localVelocity.y * (1f - verticalDrag), localVelocity.z * (1f - planarDrag));
        newVelocity = transform.TransformDirection(newVelocity);

        //testGlobal = rb.velocity;
        //testLocal = transform.InverseTransformDirection(testGlobal);
        rb.velocity = newVelocity;
    }
}

//the softbody behaves in a weird way when walking on a sphere (seemingly jumpin every time he moves onto another face). The reason for that is that the velocity is stored in world space.
// -> I should be able to save the local position at the end of the function and set the current velocity to that the next time the function starts.