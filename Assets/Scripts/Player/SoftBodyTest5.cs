﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoftBodyTest5 : MonoBehaviour
{
    Ray r;
    Ray l;
    Ray u;
    Ray d;

    RaycastHit rHit;
    RaycastHit lHit;
    RaycastHit uHit;
    RaycastHit dHit;

    float rPressure;
    float dPressure;

    [SerializeField]
    AnimationCurve pressureCurve;

    Mesh mesh;

    Vector3[] vertices, startVertices, targetVertices, velocities, vertexOffset;

    float meshRadius;
    public float collRadius;

    float squashForce = 3f; //0.5f
    float stretchForce = 7f; //1.4f

    float maxInfluenceDistance = 1.8f; //1.4f    //Has to scale with size.

    bool test;

    float maxVertexAccelleration = 0.5f; //1.5f
    float springForce = 1f; //0.5f
    float damp = 0.03f; //0.01f
    float maxVertexVelocity = 0.4f; //0.4f

    enum Direction { x, y, z };

    Transform orientation;

    Vector3 direction;

    List<Collider> others;

    private void Awake()
    {
        orientation = transform.GetChild(0);

        mesh = GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
        startVertices = mesh.vertices;
        targetVertices = mesh.vertices;
        velocities = new Vector3[vertices.Length];
        vertexOffset = new Vector3[vertices.Length];

        meshRadius = transform.lossyScale.x / 2f;
        collRadius = GetComponent<SphereCollider>().radius;

        others = new List<Collider>();
    }


    void Update()
    {
        MoveVertices();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (Collider other in others)
            {
                print(other.ClosestPoint(transform.position) - transform.position);
            }
        }

        Debug.DrawRay(transform.position, transform.up, Color.black);
    }



    private void MoveVertices()
    {
        for (int i = 0; i < vertices.Length; i++)
        {
            vertexOffset[i] = Vector3.zero;

            foreach(Collider other in others)
            {
                direction = other.ClosestPoint(transform.position) - transform.position;
                orientation.LookAt(transform.position + direction);
                //orientation.LookAt(transform.InverseTransformDirection(transform.position + direction));

                if (direction.magnitude > 0.1f)
                {
                    float pressure = 1 - (direction.magnitude / meshRadius);
                    pressure = pressureCurve.Evaluate(pressure);

                    SquashAndStretch(i, direction.normalized, pressure);
                } 
            }

            targetVertices[i] = startVertices[i] + vertexOffset[i];
            Vector3 moveDir = targetVertices[i] - vertices[i];
            velocities[i] += Vector3.ClampMagnitude(moveDir, maxVertexAccelleration);
            Vector3 deformation = startVertices[i] - vertices[i];
            velocities[i] += deformation * springForce;
            velocities[i] = Vector3.ClampMagnitude(velocities[i], maxVertexVelocity);
            velocities[i] *= 1f - damp;

            //velocities[i] = transform.InverseTransformDirection(velocities[i]); //causes a very funny bug

            vertices[i] += velocities[i] * Time.deltaTime;
        }
        mesh.vertices = vertices;
    }

    private void SquashAndStretch(int i, Vector3 distance, float pressure)
    {
        float influenceDistance = ((distance.normalized * meshRadius) - startVertices[i]).magnitude;
        if (influenceDistance > maxInfluenceDistance)
        {
            influenceDistance = maxInfluenceDistance;
        }
        float mappedInfluence = influenceDistance / maxInfluenceDistance;

        float influenceFactor = 1 - mappedInfluence;
        influenceFactor = Mathf.Clamp(influenceFactor, 0, 0.75f);

        vertexOffset[i] -= distance * influenceFactor * pressure * squashForce;
        Vector3 stretchDirection = startVertices[i];

        stretchDirection = orientation.InverseTransformDirection(stretchDirection);

        stretchDirection.z = 0f;

        stretchDirection = orientation.TransformDirection(stretchDirection);

        //targetVertices[i] += stretchDirection * influenceFactor * pressure * stretchForce;
        vertexOffset[i] += stretchDirection * influenceFactor * pressure * stretchForce;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!others.Contains(other))
        {
            others.Add(other);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //direction = other.ClosestPoint(transform.position) - transform.position;



    }

    private void OnTriggerExit(Collider other)
    {
        if (others.Contains(other))
        {
            others.Remove(other);
        }
    }
}

//CollRadius is currently unused.
