﻿using UnityEngine;

public class Move : MonoBehaviour
{
    Transform player;
    float collRadius;

    float thisWidth = 0.5f;

    public bool lessThanPlayerPos;

    public KeyCode positive;
    public KeyCode negative;

    public Vector3 moveDir;
    public float speed = 0.01f;

    bool stop1 = false;
    public bool stop2 = false;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        collRadius = player.GetComponent<SphereCollider>().radius;

        if(transform.position.x - player.position.x < 0f)
        {
            lessThanPlayerPos = true;
        }
        else if (transform.position.y - player.position.y < 0f)
        {
            lessThanPlayerPos = true;
        }
    }

    void Update()
    {
        float distance = (transform.position - player.position).magnitude;
        if (lessThanPlayerPos)
        {
            if (distance < thisWidth)
            {
                stop1 = true;
            }
            else
            {
                stop1 = false;
            } 
        }
        else
        {
            if (distance < thisWidth)
            {
                stop2 = true;
            }
            else
            {
                stop2 = false;
            }
        }

        if (Input.GetKey(positive) && !stop1)
        {
            transform.Translate(moveDir * speed);
        }
        if (Input.GetKey(negative) && !stop2)
        {
            transform.Translate(moveDir * -speed);
        }
    }
}
