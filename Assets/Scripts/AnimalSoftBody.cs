﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSoftBody : MonoBehaviour
{
    Mesh[] meshes;
    MeshFilter[] filters;

    MeshFilter filter;
    Mesh mesh;
    

    [SerializeField]
    LayerMask mask;

    Vector3[] vertices, startVertices, targetVertices, velocities, vertexOffset, vertexStartNormals;

    float rayOffset = 0.1f;
    float downRayLength = 0.35f; //should be the distance to the lowest point of the mesh + rayOffset.

    float pressure;

    public bool hitGround;

    float stretchAmount = 0.5f;

    public Vector3 localHitPoint;

    float maxForce = 5f;
    float springForce = 5f;

    private void Awake()
    {
        //MeshCombine();
        mesh = GetComponent<MeshFilter>().mesh;
        SetArrays();
    }

    void MeshCombine()
    {
        filters = GetComponentsInChildren<MeshFilter>();
        meshes = new Mesh[filters.Length];

        CombineInstance[] combine = new CombineInstance[filters.Length];

        for (int i = 0; i < filters.Length; i++)
        {
            combine[i].mesh = filters[i].sharedMesh;
            combine[i].transform = filters[i].transform.localToWorldMatrix;
            filter = transform.GetComponent<MeshFilter>();
            filter.mesh = mesh = new Mesh();

            filter.mesh.CombineMeshes(combine);
        }
    }
    void SetArrays()
    {
        vertices = mesh.vertices;
        startVertices = mesh.vertices;
        targetVertices = mesh.vertices;
        velocities = new Vector3[vertices.Length];
        vertexOffset = new Vector3[vertices.Length];
        vertexStartNormals = mesh.normals;
    }

    void Update()
    {
        CheckGround();
        if (hitGround)
        {
            SetTargetVertices();
        }
        else
        {
            //ResetTargetVertices();
            //SetTargetVertices();
        }
    }


    private void CheckGround()
    {
        Ray ray = new Ray(transform.position + transform.up * rayOffset, -transform.up);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, downRayLength, mask, QueryTriggerInteraction.Ignore))
        {
            float distance = hit.distance;
            pressure = 1 - (distance / (downRayLength - rayOffset));

            localHitPoint = transform.InverseTransformDirection(hit.point);

            hitGround = true;
        }
        else
        {
            hitGround = false;
        }

        MoveVertices();
    }

    private void MoveVertices()
    {

        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 velocity = targetVertices[i] - vertices[i];
            velocity = Vector3.ClampMagnitude(velocity, maxForce);

            Vector3 deformation = startVertices[i] - vertices[i];
            velocity += deformation * springForce;

            velocities[i] += velocity;
            vertices[i] += velocity;
        }
    }

    void ResetTargetVertices()
    {
        for (int i = 0; i < vertices.Length; i++)
        {
            targetVertices[i] = startVertices[i];
        }
    }

    private void SetTargetVertices()
    {
        float maxInfluenceFactor = 1f;
        float minInfluenceFactor = 0f;

        for (int i = 0; i < vertices.Length; i++)
        {
            targetVertices[i] = startVertices[i];
            if(targetVertices[i].y < localHitPoint.y)
            {
                targetVertices[i].y = localHitPoint.y;
            }

            float influenceFactor = 1 / Mathf.Abs(startVertices[i].y - localHitPoint.y);
            influenceFactor = Mathf.Clamp(influenceFactor, minInfluenceFactor, maxInfluenceFactor);

            Vector3 planarOffset = vertexStartNormals[i] * influenceFactor * stretchAmount;
            planarOffset.y = 0f;

            targetVertices[i] += planarOffset;



        }

        mesh.vertices = targetVertices; //Just for quick testing!

    }
}
