﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CoroutineTest : MonoBehaviour
{
    public int myInt;


    IEnumerator IntIncrease(Action<int> intCallback, int thisInt)
    {
        while (true)
        {
            thisInt++;

            intCallback(thisInt);
            yield return new WaitForSeconds(1f);
        }

    }

    void SetInt(int anotherInt)
    {
        //myInt = anotherInt;
    }

    void Start()
    {
        //StartCoroutine(IntIncrease(result => myInt => result, myInt));
        StartCoroutine(IntIncrease((result) =>
        {
            myInt = result;
        }, myInt));
    }
}
