﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptedShape : MonoBehaviour
{
    public float squashAmount;
    Transform mesh;

    Rigidbody rb;

    public Vector3 hitDirection;

    void Start()
    {
        mesh = transform.GetChild(0).transform;
        rb = GetComponentInChildren<Rigidbody>();

       // Squash(hitDirection);
    }

    private void Update()
    {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        Vector3 moveVector = new Vector3(h, 0f, v);;
        rb.AddForce(moveVector.normalized * 10f);

        if (moveVector.magnitude > 0.5f)
            mesh.transform.rotation = Quaternion.LookRotation(moveVector, Vector3.up);
    }

    public void ChildCollision (Collision other)
    {
        Squash(other.contacts[0].normal, squashAmount);
    }

    public void ChildCollisionExit()
    {
        Squash(Vector3.zero, 0f);
    }


    private void Squash(Vector3 dir, float amount)
    {
        print(dir);
        dir = dir.normalized;

        Quaternion currentMeshRotation = mesh.rotation;

        //I may want to disable the collider for just the next two lines. 
        //Rigidbody could create problems too but freezing it may reset velocity which I'd have to save in a variable and then apply.
        transform.position = mesh.position;
        mesh.localPosition = Vector3.zero;

        transform.LookAt(transform.position + dir);
        transform.localScale = new Vector3(1f, 1f, 1 - amount);

        mesh.rotation = currentMeshRotation;
    }
}
