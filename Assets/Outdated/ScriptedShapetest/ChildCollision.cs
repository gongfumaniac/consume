﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildCollision : MonoBehaviour {

    private void OnCollisionEnter(Collision other)
    {
        transform.parent.GetComponent<ScriptedShape>().ChildCollision(other);
    }

    private void OnCollisionExit(Collision collision)
    {
        transform.parent.GetComponent<ScriptedShape>().ChildCollisionExit();
    }
}
